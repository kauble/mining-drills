#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  query-house.py
#
#  Usage: python3 query-house.py input.html results.csv

#  Copyright 2018 Kyle Auble <kauble7@gmail.com>
#
#  Permission is hereby granted, free of charge, to any person obtaining
#  a copy of this software and associated documentation files (the
#  “Software”), to deal in the Software without restriction, including
#  without limitation the rights to use, copy, modify, merge, publish,
#  distribute, sublicense, and/or sell copies of the Software, and to
#  permit persons to whom the Software is furnished to do so, subject to
#  the following conditions:
#
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
#  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
#  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.

# This quick-and-dirty script was written to help build a linear-
# regression model to check some property appraisals.
#
# It is designed to data-mine public appraisal records from a certain US
# county's auditor website and might not be particularly generalizable.
#
# Looping and downloading files is also simple enough to handle in the
# shell so each execution only handles one file.
#
# After setting a control flag below, it will look for details from one
# of a few page types, then return the file's data as a CSV record that
# can be appended through the shell.
#
# The data and fields from different page types can also be collated in
# a spreadsheet program easily enough.

# First, for the requirements, the main one being Beautiful Soup
# Since it's a stand-alone program, sys is imported at bottom
from bs4 import BeautifulSoup

# We can automate much of the exporting by using the CSV library
import csv
# To keep the CSV file consistent, let's use an OrderedDict
from collections import OrderedDict
# For checking if the CSV file needs a header or not
from os import stat
# For looking at the improvements list & nonstandard statuses
import re

# Since we may need to scan different pages in batches...
# let's introduce a control flag for now

pagetype = 'summary'
#pagetype = 'homebasics'
#pagetype = 'landbasics'

# We'll go ahead and start our dictionary as a global too
houserecord = OrderedDict({ 'parcel': None })

def openhtml(htmlfile):
    """Load the html file into a Beautiful Soup object"""

    # First we need to open the file
    with open(htmlfile) as naivehtml:
        # Then making the soup is simple enough
        soup = BeautifulSoup(naivehtml, 'lxml')

    return soup


def writerecord(csvfile):
    """Open the CSV file and export the full dictionary to it"""

    # First open the file
    with open(csvfile, 'a') as tables:

        # Next, we'll define our dictionary writer
        exporter = csv.DictWriter(tables, sorted(houserecord))

        # Then we check if the file is empty and needs a header
        if stat(csvfile).st_size is 0:
            exporter.writeheader()

        # Then we export the actual record; make sure it's sorted!
        exporter.writerow(houserecord)

        # And with that we should be done


def scansummary(soup):
    """Scan the summary page & put what we need into a dictionary"""

    # Here's where we get into the nitty-gritty

    # First, we need the parcel id
    houserecord['parcel'] = findaunt(soup, "Parcel ID")

    # Next, we want owner & contact info
    houserecord['owner'] = findaunt(soup, "Owner Name", 'firstline')
    houserecord['contact'] = findaunt(soup, "Mailing Name", 'firstline')

    # Now the deed type & acreage
    houserecord['deed'] = findaunt(soup, "Deed Type")
    houserecord['acreage'] = findaunt(soup, "Acreage", 'float')

    # Now the various specials statuses
    houserecord['revboard'] = findaunt(soup, "Board of Revision", 'boolean')
    houserecord['rental'] = findaunt(soup, "Rental Registration", 'boolean')
    houserecord['homestead'] = findaunt(soup, "Homestead", 'boolean')
    houserecord['ownoccupy'] = findaunt(soup, "Owner Occupancy", 'boolean')
    houserecord['foreclosed'] = findaunt(soup, "Foreclosure", 'boolean')

    # OK, and now the valuation stuff
    houserecord['colandval'] = findaunt(soup, "Market Land Value", 'acct')
    houserecord['cauv'] = findaunt(soup, "CAUV Value", 'acct')
    houserecord['coimpval'] = findaunt(soup, "Market Improvement Value", 'acct')
    houserecord['taxes'] = findaunt(soup, "Total Tax", 'acct')

    # We normally don't pull weird tax add-ons, but open land needs more data points
    houserecord['tif'] = findaunt(soup, "TIF Value", 'acct')
    houserecord['abated'] = findaunt(soup, "Abated Value", 'acct')
    houserecord['exempt'] = findaunt(soup, "Exempt Value", 'acct')


def scanhome(soup):
    """Scan the appraisal detail page & put it in a dictionary"""

    # The guts of the other home info page

    # We need the parcel id again (primary key)
    houserecord['parcel'] = findaunt(soup, "Parcel ID")

    # Now the outside physical descriptors
    houserecord['style'] = findaunt(soup, "Style")
    houserecord['grade'] = findaunt(soup, "Grade")
    houserecord['exterior'] = findaunt(soup, "Exterior Wall")
    houserecord['stories'] = findaunt(soup, "Stories", 'float')

    # Details about the basement
    houserecord['basetype'] = findaunt(soup, "Basement Type")
    houserecord['basecar'] = findaunt(soup, "Basement Garage", 'float')
    houserecord['finbase'] = findaunt(soup, "Finished Basement", 'acct')

    # Finally, look for some improvements
    # NOTE: For now, I just see if they have a shed or not...
    #           I don't look for anything else or use measurements

    hits = soup.find_all(string="Improvement")

    # A few houses have no listed improvements
    if not hits:
        houserecord['shed'] = False
        return
    else:
        landmark = hits[-1]

    # It looks like we need to get 3 levels above this
    improvements = list(landmark.parents)[2]

    if improvements.find(string = re.compile("Shed")) is None:
        houserecord['shed'] = False
    else:
        houserecord['shed'] = True


def scanland(soup):
    """Scan the appraisal details page for open land"""

    # Finally, the guts of land appraisal details...
    #   I wound up not using this, but kept the stub just in case

    # As usual, take the Parcel ID for a primary key
    houserecord['parcel'] = findaunt(soup, "Parcel ID")


def findaunt(soup, querystring, processing='strip', steps = 2):
    """Find a string, jump to its aunt N steps away, then clean some"""

    # First find the string
    landmark = soup.find(string=re.compile(querystring, re.I)).parent
    # Sometimes the string's in an anchor and we need a great aunt
    if landmark.name is 'a':
        landmark = landmark.parent

    # Now we're at the proper level, step across siblings
    landmark = list(landmark.next_siblings)[steps-1]
    # And if the value is straight-forward, grab it...
    if landmark.string is not None:
        dirtystring = landmark.string
    # But sure enough, sometimes the value we want is in an anchor...
    else:
        dirtystring = landmark.a.string
    # NOTE: I know I'm excluding a case, but for now, it should be OK

    # Everything past here is to clean up the value to export
    if processing is 'strip':
        return dirtystring.strip()
    elif processing is 'firstline':
        return dirtystring.split('\n')[0]
    elif processing is 'float':
        return float(dirtystring)
    elif processing is 'acct':
        tmpstring = dirtystring.strip('$')
        return float(tmpstring.replace(',',''))
    elif processing is 'boolean':
        if re.match("No", dirtystring, re.I):
            return False
        elif re.match("Yes", dirtystring, re.I):
            return True
        else:
            return None
    else:
        return dirtystring


if __name__ == '__main__':
    import sys

    # Add some low-hanging input validation
    if len(sys.argv) is not 3:
        sys.exit("You should have 2 and only 2 parameters: the html file and an output CSV")

    # First just open and soupify the file
    soup = openhtml(str(sys.argv[1]))

    # Now let's load a dictionary with what we want
    # The signal-to-noise ratio is pretty high; no need to strain first

    # However, we'll choose our logic based on the page type
    if pagetype is 'summary':
        scansummary(soup)
    elif pagetype is 'homebasics':
        scanhome(soup)
    elif pagetype is 'landbasics':
        scanland(soup)

    # Then we export the full house record to the CSV
    writerecord(sys.argv[2])

    sys.exit(0)
