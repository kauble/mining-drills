# Old Data-Mining Drills

This is just a catch-all repository for little scripts I've written in
data-mining projects. Basic descriptions and notes are included at the
top of each script.

There should be licensing info at the top of each file too, but I'm
releasing everything here under the MIT license by default. The scripts
are very specific so they're likely not reusable; I'm mainly just
recording them for posterity.

If for some reason, you have questions or are interested in working them
into a project of your own, feel free to contact me at:

Kyle Auble <kyle.auble@outlook.com>
